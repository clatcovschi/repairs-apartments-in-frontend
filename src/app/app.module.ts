import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import {CommonModule} from '@angular/common';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {NgbModule} from "@ng-bootstrap/ng-bootstrap";
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {SlickCarouselModule} from "ngx-slick-carousel";
import {FormBuilder, FormsModule, ReactiveFormsModule} from "@angular/forms";
import {CookieService} from "ngx-cookie-service";
import {NbAuthModule, NbDummyAuthStrategy} from "@nebular/auth";
import {AccessTokenService} from "./admin/consts/access-token.service";
import {LandingModule} from "./landing/landing.module";
import {MiscellaneousModule} from "./@theme/components/miscellaneous/miscellaneous.module";

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    NbAuthModule.forRoot({
      strategies: [
        NbDummyAuthStrategy.setup({
          name: 'email',

          alwaysFail: true,
          delay: 1000,
        }),
      ],
    }),
    CommonModule,
    BrowserModule,
    NgbModule,
    HttpClientModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    SlickCarouselModule,
    FormsModule,
    ReactiveFormsModule,
    LandingModule,
    MiscellaneousModule

  ],
  providers: [CookieService, AccessTokenService, FormBuilder
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
