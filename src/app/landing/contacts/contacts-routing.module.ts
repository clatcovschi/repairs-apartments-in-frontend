import {RouterModule, Routes} from '@angular/router';
import {NgModule} from "@angular/core";
import {
  ResolveService1
} from "../../resolve.service";
import {ContactsComponent} from "./contacts.component";

const routes: Routes = [
  {
    path: '',
    component: ContactsComponent,
    data: {name: 'Contacts'},
    resolve: {
      routeResolver1: ResolveService1
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ContactsRoutingModule {
}

export const routedComponents = [
  ContactsComponent
];

