import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {ABOUT_US__ITEMS} from "./about-us.items";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-about-us',
  templateUrl: './about-us.component.html',
  styleUrls: ['./about-us.component.scss'],
  encapsulation: ViewEncapsulation.None,

})
export class AboutUsComponent implements OnInit {
  about_us_items = ABOUT_US__ITEMS;
  company;
  about_us;


  constructor(private route: Router, private routes: ActivatedRoute, private router: Router) {
    this.routes.data.subscribe((response: any) => {
      this.company = response.routeResolver1.docs[0]
      this.about_us = response.routeResolver9.docs[0];
    });

  }

  ngOnInit(): void {
  }

}
