import {Component, OnInit} from '@angular/core';
import {BLOG_ITEMS} from "./blog.items";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.scss']
})
export class BlogComponent implements OnInit {


  blog_items = BLOG_ITEMS;
  company;
  blogs;

  constructor(private routes: ActivatedRoute) {
    this.routes.data.subscribe((response: any) => {

      this.company = response.routeResolver1.docs[0];
      this.blogs = response.routeResolver6.docs;

    })

  }

  ngOnInit(): void {
  }

}
