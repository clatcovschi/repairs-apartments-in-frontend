import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HomeComponent} from './home.component';
import {HomeService} from "./home.service";
import {SharedModule} from "../shared/shared.module";
import {HomeRoutingModule} from "./home-routing.module";
import {SlickCarouselModule} from "ngx-slick-carousel";
import {PipesModule} from "../../pipes/pipes.module";
import {NgxImageGalleryModule} from '@web-aid-kit/ngx-image-gallery';


@NgModule({
  declarations: [HomeComponent],
  imports: [
    CommonModule,
    HomeRoutingModule,
    SharedModule,
    SlickCarouselModule,
    PipesModule,
    NgxImageGalleryModule

  ],
  providers: [
    HomeService
  ]
})
export class HomeModule {
}
