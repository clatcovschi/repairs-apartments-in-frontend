import { Component, OnInit } from '@angular/core';
import {HomeService} from "../../home/home.service";
import {ActivatedRoute, Event, NavigationEnd, NavigationStart, Router} from "@angular/router";

@Component({
  selector: 'app-spinner',
  templateUrl: './spinner.component.html',
  styleUrls: ['./spinner.component.scss']
})
export class SpinnerComponent implements OnInit {
  showSpinner = false;

  constructor(private service: HomeService, private routes: ActivatedRoute, private router: Router) {

    this.router.events.subscribe((routerEvent: Event) => {
      if (routerEvent instanceof NavigationStart) {
        this.showSpinner = true;
      }

      if (routerEvent instanceof NavigationEnd) {
        this.showSpinner = false;
      }
    })

  }

  ngOnInit(): void {
  }

}
