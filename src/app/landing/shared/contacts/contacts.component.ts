import {AfterViewInit, Component, Input, OnInit} from '@angular/core';
import {CONTACTS_ITEMS} from "./contacts.items";
import {SharedService} from "../shared.service";
import {ActivatedRoute} from "@angular/router";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {environment} from "../../../../environments/environment";
import * as Leaflet from 'leaflet';
import * as AOS from 'aos'

@Component({
  selector: 'app-contact',
  templateUrl: './contacts.component.html',
  styleUrls: ['./contacts.component.scss']
})
export class ContactsComponent implements OnInit, AfterViewInit {

  @Input() map_title;
  @Input() home;
  map: Leaflet.Map;
  company;
  show_toast = false;
  contacts_items = CONTACTS_ITEMS;
  submitted = false;
  coordinates;
  imageUrl = environment.api_url + '/api/';
  mapIcon;


  formMessage: FormGroup;

  data = {};

  constructor(private service: SharedService, private routes: ActivatedRoute,
              private formBuilder: FormBuilder
  ) {

    this.routes.data.subscribe((response: any) => {
      this.company = response.routeResolver1.docs[0];
      this.coordinates = this.company.coordinates.split(';');

    })

  }

  ngAfterViewInit() {
    this.map = Leaflet.map(this.map_title).setView([this.coordinates[0] ? this.coordinates[0] : 47.43, this.coordinates[1] ? this.coordinates[1] : 27.34], 10);
    Leaflet.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {}).addTo(this.map);
    this.mapIcon = Leaflet.icon({
      iconUrl: 'http://coffeein.md/assets/img/icons/map-marker.png',

      iconSize: [16, 16],
      popupAnchor: [0, -8]
    });

    Leaflet.marker([this.coordinates[0], this.coordinates[1]], {icon: this.mapIcon}).addTo(this.map).bindPopup(this.coordinates[2] ? this.coordinates[2] : this.contacts_items.map_location).openPopup();

  }

  ngOnInit(): void {

    AOS.init({
      once: true
    });

    this.formMessage = this.formBuilder.group({
      name: [''],
      phone: ['', [Validators.required]],
      mail: [''],
      message: ['', [Validators.required]],

    });


  }


  onSubmit() {


    this.submitted = true;

    if (this.f.phone.errors && this.f.message.errors) {
      alert(this.contacts_items.both_required);

    } else if (this.f.phone.errors) {
      alert(this.contacts_items.phone_required)

    } else if (this.f.message.errors) alert(this.contacts_items.message_required)

    this.data = {
      name: this.formMessage.value.name === '' ? 'Имя не указано' : this.formMessage.value.name,
      phone: this.formMessage.value.phone,
      mail: this.formMessage.value.mail === '' ? 'Электронная почта не указана' : this.formMessage.value.mail,
      message: this.formMessage.value.message
    }

    if (this.formMessage.invalid) {
      return;
    } else {
      this.service.postMessages(this.data).subscribe(data => {

        setTimeout(data => {
          this.show_toast = false;

        }, 2000)

        this.formMessage = this.formBuilder.group({
          name: [''],
          phone: ['', [Validators.required]],
          mail: [''],
          message: ['', [Validators.required]],

        });
      })

      this.service.postMessageOnEmail(this.data).subscribe(data => {
        this.show_toast = true;

      });


    }


  }

  get f() {
    return this.formMessage.controls;
  }


  isClicked() {
    this.show_toast = false;
  }
}
