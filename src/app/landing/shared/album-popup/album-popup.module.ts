import {InjectionToken, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {
  NbButtonModule,
  NbCardModule,
  NbDialogService, NbLayoutModule,
} from "@nebular/theme";
import {AlbumPopupComponent} from "./album-popup.component";
import {AlbumPopupRoutingModule} from "./album-popup-routing.module";
import {SlickCarouselModule} from "ngx-slick-carousel";
import {NgxImageGalleryModule} from "@web-aid-kit/ngx-image-gallery";


@NgModule({
  declarations: [AlbumPopupComponent],
  imports: [
    CommonModule,
    AlbumPopupRoutingModule,
    NbCardModule,
    NbButtonModule,
    NbLayoutModule,
    SlickCarouselModule,
    NgxImageGalleryModule
  ],
  providers: [NbDialogService]
})
export class AlbumPopupModule {
}
