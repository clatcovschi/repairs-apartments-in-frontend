import {AfterViewInit, Component, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {NbDialogService} from '@nebular/theme';
import {ActivatedRoute, Router} from "@angular/router";
import {GALLERY_CONF, GALLERY_IMAGE, NgxImageGalleryComponent} from "@web-aid-kit/ngx-image-gallery";
import {Location} from "@angular/common";
import {environment} from "../../../../environments/environment";

@Component({
  selector: 'app-album-popup',
  templateUrl: './album-popup.component.html',
  styleUrls: ['./album-popup.component.scss']
})
export class AlbumPopupComponent implements OnInit, AfterViewInit {

  @ViewChild('dialog') dialog: TemplateRef<any>;
  @ViewChild(NgxImageGalleryComponent) ngxImageGallery: NgxImageGalleryComponent;

  // gallery configuration
  conf: GALLERY_CONF = {
    imageOffset: '30px',
    showDeleteControl: false,
    showImageTitle: true,
  };

  // gallery images
  images: GALLERY_IMAGE[] = [];
  company;
  imageUrl = environment.api_url + '/api/';
  album;
  return = true;
  album_id;
  photo_albums;
  album_length: number;
  show_popup = true;
  index;
  max_index;
  slideConfig = {
    slidesToShow: 2,
    slidesToScroll: 2,
    autoplay: false,
    arrows: true,
    dots: false,
    infinite: true,
    speed: 1000,
    responsive: [
      {
        breakpoint: 1270,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          autoplay: false
        }
      },
      {
        breakpoint: 767,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          autoplay: true,
          arrows: false
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          autoplay: true,
          arrows: false

        }
      }
    ]

  };


  breakpoint(e) {
  }

  constructor(private dialogService: NbDialogService, private routes: ActivatedRoute, private router: Router, private location: Location) {

    this.routes.params.subscribe(params => {
      this.album_id = params['id'];
    });

    this.routes.data.subscribe((response: any) => {

      this.company = response.routeResolver1.docs[0];
      this.photo_albums = response.routeResolver2.docs;

      for (let i = 0; i < this.photo_albums.length; i++) {
        if (this.photo_albums[i]._id === this.album_id) {
          this.album = this.photo_albums[i];
          this.return = false;
          this.album_id = undefined;
          this.album_length = this.photo_albums[i].albums.length;
          for (let j = 0; j < this.photo_albums[i].albums.length; j++) {
            this.images.push({
              url: this.imageUrl + this.photo_albums[i].albums[j].mainProductImage.path,
              extUrl: this.imageUrl + this.photo_albums[i].albums[j].mainProductImage.path,
              altText: 'Здесь нет картинок',
              title: this.photo_albums[i].albums[j].title,
              thumbnailUrl: this.imageUrl + this.photo_albums[i].albums[j].mainProductImage.path

            })
          }

        }

      }
    });

    routes.queryParams.subscribe(p => {

      if (p.AlbumGalleryId) {
        this.index = Number(p.AlbumGalleryId) - 1;
        this.max_index = Number(this.album.albums.length) - 1;
      }

    })

  }


  ngOnInit(): void {

    if (this.return) {
      this.location.go('/photo-albums');
      this.return = false;
      this.album_id = undefined;

    }

  }

  ngAfterViewInit() {
    this.routes.params.subscribe(params => {
      this.album_id = params['id'];


      for (let i = 0; i < this.photo_albums.length; i++) {
        if (this.photo_albums[i]._id === this.album_id) {
          this.open(this.dialog);

        }

      }
    });

    if (this.index >= 0 && this.index <= this.max_index) {
      this.openGallery(this.index);
    } else {
      this.router.navigate(['.'], {relativeTo: this.routes});
    }


  }


  open(dialog: TemplateRef<any>) {
    this.dialogService.open(dialog, {closeOnBackdropClick: false, closeOnEsc: false, hasScroll: true});
  }

  closeDialog() {
    this.router.navigate(['/photo-albums']);
    this.return = false;
    this.album_id = undefined;

  }


  openGallery(index: number = 0) {

    setTimeout(() => {
      this.ngxImageGallery.open(index);

    }, 1)

  }

  // close gallery
  closeGallery() {
    this.ngxImageGallery.close();

  }

  // set new active(visible) image in gallery
  newImage(index: number = 0) {
    this.ngxImageGallery.setActiveImage(index);
  }

  // next image in gallery
  nextImage(index: number = 0) {
    this.ngxImageGallery.next();
  }

  // prev image in gallery
  prevImage(index: number = 0) {
    this.ngxImageGallery.prev();
  }

  /**************************************************/

  // EVENTS
  // callback on gallery opened


  // callback on gallery closed
  galleryClosed() {
    this.router.navigate(['.'], {relativeTo: this.routes});
  }

  // callback on gallery image clicked
  galleryImageClicked(index) {

  }

  galleryOpened(index) {
    this.router.navigate(['.'], {relativeTo: this.routes, queryParams: {AlbumGalleryId: index + 1}});

  }


  // callback on gallery image changed
  galleryImageChanged(index) {

    this.router.navigate(['.'], {relativeTo: this.routes, queryParams: {AlbumGalleryId: index + 1}});

  }

}
