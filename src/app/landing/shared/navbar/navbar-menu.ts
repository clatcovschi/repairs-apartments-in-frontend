export const NAVBAR_ITEMS = [
  {
    title: 'ГЛАВНАЯ',
    link: '',
  },
  {
    title: 'УСЛУГИ',
    link: '/services',
  },
  {
    title: 'ФОТООТЧЕТ',
    link: '/photo-albums',
  },
  {
    title: 'О НАС',
    link: '/about-us',
  },
  {
    title: 'БЛОГ',
    link: '/blog',
  },
  {
    title: 'КОНТАКТЫ',
    link: '/contacts',
  },
];
