import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {NavbarComponent} from './navbar/navbar.component';
import {FooterComponent} from './footer/footer.component';
import {SharedService} from "./shared.service";
import {RouterModule} from "@angular/router";
import {HomeNavbarComponent} from './home-navbar/home-navbar.component';
import {PipesModule} from "../../pipes/pipes.module";
import {GoToTopComponent} from './go-to-top/go-to-top.component';
import {BlogComponent} from './blog/blog.component';
import {ContactsComponent} from './contacts/contacts.component';
import {SpinnerComponent} from './spinner/spinner.component';
import {FormBuilder, FormsModule, ReactiveFormsModule} from "@angular/forms";
import {NgxImageGalleryModule} from "@web-aid-kit/ngx-image-gallery";


@NgModule({
  declarations: [NavbarComponent, FooterComponent, HomeNavbarComponent, GoToTopComponent, BlogComponent, ContactsComponent, SpinnerComponent],
  imports: [
    CommonModule,
    RouterModule,
    PipesModule,
    FormsModule,
    ReactiveFormsModule,
    NgxImageGalleryModule,


  ],
  exports: [
    NavbarComponent,
    FooterComponent,
    HomeNavbarComponent,
    GoToTopComponent,
    BlogComponent,
    ContactsComponent,
    SpinnerComponent,
  ],
  providers: [
    SharedService,
    FormBuilder
  ]
})
export class SharedModule {
}
