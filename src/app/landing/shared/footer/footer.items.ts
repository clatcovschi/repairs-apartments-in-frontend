export const FOOTER_ITEMS = {
  social_networks: "Социальные сети",
  whatsapp: "WhatsApp",
  contact_us: "Свяжитесь с нами",
  address: "Адрес",
  phone: "Телефон",
  mail: "Электронная почта",
  working_hours: "ЧАСЫ РАБОТЫ"
}
