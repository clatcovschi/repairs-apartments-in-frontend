import {AfterViewInit, Component, ElementRef, HostListener, OnInit, ViewChild} from '@angular/core';
import {SharedService} from "../shared.service";
import {environment} from "../../../../environments/environment";
import {HOME_NAVBAR_ITEMS} from "./home-navbar-items";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-home-navbar',
  templateUrl: './home-navbar.component.html',
  styleUrls: ['./home-navbar.component.scss']
})
export class HomeNavbarComponent implements OnInit, AfterViewInit {

  company;
  imageUrl = environment.api_url + '/api/';
  height;
  home_navbar = HOME_NAVBAR_ITEMS;
  advantages;

  constructor(private service: SharedService, private routes: ActivatedRoute) {
    this.routes.data.subscribe((response: any) => {
      this.company = response.routeResolver1.docs[0]
      this.advantages = response.routeResolver8.docs
    })
  }

  @ViewChild('navbar') elementView: ElementRef;

  contentHeight: number;

  ngAfterViewInit() {
    this.contentHeight = this.elementView.nativeElement.offsetHeight;
  }



  @HostListener('window:scroll')
  checkScroll() {
    const scrollPosition = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0;
    this.height = Number(scrollPosition);
  }


  ngOnInit(): void {

  }

}
