import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {BlogPageComponent} from './blog-page.component';
import {BlogPageRoutingModule} from "./blog-page-routing.module";
import {SharedModule} from "../shared/shared.module";
import {PipesModule} from "../../pipes/pipes.module";


@NgModule({
  declarations: [BlogPageComponent],
  imports: [
    CommonModule, BlogPageRoutingModule, SharedModule, PipesModule
  ]
})
export class BlogPageModule {
}
