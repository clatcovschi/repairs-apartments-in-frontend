import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-blog-page',
  templateUrl: './blog-page.component.html',
  styleUrls: ['./blog-page.component.scss'],
  encapsulation: ViewEncapsulation.None,

})
export class BlogPageComponent implements OnInit {

  blog_param;
  blog;
  blogs;
  company;
  return = true;

  constructor(private route: Router, private routes: ActivatedRoute, private router: Router) {
    this.routes.data.subscribe((response: any) => {
      this.company = response.routeResolver1.docs[0]
      this.blogs = response.routeResolver6.docs;
    });

    this.routes.params.subscribe(params => {
      this.blog_param = params['id'];
      for (let i = 0; i < this.blogs.length; i++) {
        if (this.blogs[i].slug === this.blog_param) {
          this.blog = this.blogs[i];
          for (let i = 0; i < this.blogs.length; i++) {

            if (this.blogs[i]._id === this.blog._id) {
              this.return = false;
              break;
            }

          }

        }
        if (this.blogs[i]._id === this.blog_param) {
          this.blog = this.blogs[i];
          for (let i = 0; i < this.blogs.length; i++) {

            if (this.blogs[i]._id === this.blog._id) {
              this.return = false;
              break;
            }

          }

        }
      }
    });
  }

  ngOnInit(): void {
    if (this.return) {
      this.router.navigate(['blog'])
    }
  }

}
