import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, RouterStateSnapshot, Resolve, Router} from "@angular/router";
import {empty, Observable, of} from "rxjs";
import {HomeService} from "./landing/home/home.service";
import {catchError} from "rxjs/operators";
import {SharedService} from "./landing/shared/shared.service";


@Injectable({
  providedIn: 'root'
})

export class ResolveService1 implements Resolve<any> {


  constructor(public service: HomeService) {
  }

  resolve() {
    return this.service.getCompany().pipe(
      catchError((error) => {
        return empty();
      })
    )

  }
}

@Injectable({
  providedIn: 'root'
})

export class ResolveService2 implements Resolve<any> {


  constructor(public service: HomeService) {
  }

  resolve() {
    return this.service.getAlbums().pipe(
      catchError((error) => {
        return empty();
      })
    )

  }
}

@Injectable({
  providedIn: 'root'
})


export class ResolveService3 implements Resolve<any> {


  constructor(public service: HomeService) {
  }

  resolve() {
    return this.service.getRenovations().pipe(
      catchError((error) => {
        return empty();
      })
    )

  }
}

@Injectable({
  providedIn: 'root'
})


export class ResolveService4 implements Resolve<any> {


  constructor(public service: HomeService) {
  }

  resolve() {
    return this.service.getGoals().pipe(
      catchError((error) => {
        return empty();
      })
    )

  }
}

@Injectable({
  providedIn: 'root'
})


export class ResolveService5 implements Resolve<any> {


  constructor(public service: HomeService) {
  }

  resolve() {
    return this.service.getOrganization().pipe(
      catchError((error) => {
        return empty();
      })
    )

  }
}

@Injectable({
  providedIn: 'root'
})


export class ResolveService6 implements Resolve<any> {


  constructor(public service: HomeService) {
  }

  resolve() {
    return this.service.getBlogs().pipe(
      catchError((error) => {
        return empty();
      })
    )

  }
}

@Injectable({
  providedIn: 'root'
})


export class ResolveService7 implements Resolve<any> {


  constructor(public service: HomeService) {
  }

  resolve() {
    return this.service.getServices().pipe(
      catchError((error) => {
        return empty();
      })
    )

  }
}


@Injectable({
  providedIn: 'root'
})


export class ResolveService8 implements Resolve<any> {


  constructor(public service: SharedService) {
  }

  resolve() {
    return this.service.getAdvantages().pipe(
      catchError((error) => {
        return empty();
      })
    )

  }
}

@Injectable({
  providedIn: 'root'
})


export class ResolveService9 implements Resolve<any> {


  constructor(public service: SharedService) {
  }

  resolve() {
    return this.service.getAboutUs().pipe(
      catchError((error) => {
        return empty();
      })
    )

  }
}

