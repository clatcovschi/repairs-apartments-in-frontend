import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {CreateCompanyComponent} from "./create-company/create-company.component";

const routes: Routes = [
  {
    path: '',
    component: CreateCompanyComponent,
    data: {name: 'Company'},
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsersRoutingModule {
}

export const routedComponents = [
  CreateCompanyComponent
];
