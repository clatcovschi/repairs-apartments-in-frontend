import {Component, OnInit, TemplateRef} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {AccessTokenService} from "../../consts/access-token.service";
import {HttpClient} from "@angular/common/http";
import {NbDialogService} from "@nebular/theme";
import {CompanyService} from "../company.service";
import {environment} from "../../../../environments/environment";

@Component({
  selector: 'app-create-company',
  templateUrl: './create-company.component.html',
  styleUrls: ['./create-company.component.scss']
})
export class CreateCompanyComponent implements OnInit {
  add_company = false;
  isEditPage = false;
  columns: {}
  response;
  data;
  file;
  color;
  image_src = {};
  imageUpload;
  imageContacts_src = {};
  id;
  editImage = [];
  editImageContacts = [];
  object = {
    name: '',
    phone: '',
    slug: '',
    mail: '',
    coordinates: '',
    principalColor: '',
    secondaryColor: '',
    slogan: '',
    servicePageDescription: '',
    blogPageDescription: '',
    photoAlbumsPageDescription: '',
    networksDescription: '',
    whatsapp: '',
    whatsappDescription: '',
    vk: '',
    workingHours: '',
    instagram: '',
    facebook: '',
    youtube: '',
    mainProductImage: {},
    mainProductImageContacts: {}
  };

  constructor(private dialogService: NbDialogService,
              private http: HttpClient,
              public accessToken: AccessTokenService,
              private route: ActivatedRoute,
              private router: Router,
              private service: CompanyService) {
    this.file = []
    route.queryParams.subscribe(p => {
      this.editImage = []
      this.id = p.Id;
      if (this.id !== undefined) {
        this.isEditPage = true;
        this.service.getById(this.id).subscribe(data => {
          this.object = data;
          if (data.mainProductImage && data.mainProductImage.path) {
            this.image_src = data.mainProductImage;
            this.editImage = [{
              url: `${environment.api_url}/api/` + data.mainProductImage.path.replaceAll('\\', '/'),
              fileName: data.mainProductImage.description
            }];

          }

          if (data.mainProductImageContacts && data.mainProductImageContacts.path) {
            this.imageContacts_src = data.mainProductImageContacts;
            this.editImageContacts = [{
              url: `${environment.api_url}/api/` + data.mainProductImageContacts.path.replaceAll('\\', '/'),
              fileName: data.mainProductImageContacts.description
            }];

          }


        });
      }
    });

  }

  open(dialog: TemplateRef<any>) {
    this.editImage = []
    this.editImageContacts = []
    this.image_src = [];
    this.imageContacts_src = [];
    this.dialogService.open(dialog, {});
    this.router.navigate(
      ['.'],
      {relativeTo: this.route}
    );
    this.isEditPage = false;
    this.object = {
      name: '',
      phone: '',
      slug: '',
      mail: '',
      coordinates: '',
      principalColor: '',
      secondaryColor: '',
      slogan: '',
      servicePageDescription: '',
      blogPageDescription: '',
      photoAlbumsPageDescription: '',
      networksDescription: '',
      whatsapp: '',
      whatsappDescription: '',
      vk: '',
      workingHours: '',
      instagram: '',
      facebook: '',
      youtube: '',
      mainProductImage: {},
      mainProductImageContacts: {}
    };
  }

  ngOnInit(): void {

    this.service.get().subscribe(data => {
      this.response = data.docs;
      if (data.docs.length === 0) this.add_company = true;
    });
    this.imageUpload = `${environment.api_url}/api/upload/company/image`;

    this.columns = {
      id: {
        title: 'ID'
      },
      name: {
        title: 'NAME'
      },
      phone: {
        title: 'PHONE'
      },
      mail: {
        title: 'MAIL'
      }
    }

  }

  onSubmit() {
    this.object.mainProductImage = this.image_src;
    this.object.mainProductImageContacts = this.imageContacts_src;


    if (this.isEditPage) {
      this.service.updateCompany(this.id, JSON.stringify(this.object), {headers: this.accessToken.headers}).subscribe(res => {
          this.service.get().subscribe(data => {
            this.response = data.docs;
          });
          this.service.getById(this.id).subscribe(res => {

            this.editImage = [{
              url: `${environment.api_url}/api/` + res.mainProductImage.path.replaceAll('\\', '/'),
              fileName: res.mainProductImage.description
            }];

          });

          this.service.getById(this.id).subscribe(res => {

            this.editImageContacts = [{
              url: `${environment.api_url}/api/` + res.mainProductImageContacts.path.replaceAll('\\', '/'),
              fileName: res.mainProductImageContacts.description
            }];
          });
        }
      );
    } else {
      this.add_company = false;
      this.service.createCompany(JSON.stringify(this.object), {headers: this.accessToken.headers}).subscribe(res => {
        this.service.get().subscribe(data => {
          this.response = data.docs;
        });
      });
    }

  }

  uploadFinished(event, isLogo, imageContacts) {
    if (isLogo) {
    } else {
      if (event && event.serverResponse && event.serverResponse.response && event.serverResponse.response.body && !imageContacts) {
        this.image_src = {
          path: event.serverResponse.response.body.path.replaceAll('\\', '/'),
          mimetype: event.serverResponse.response.body.mimetype,
          description: event.serverResponse.response.body.filename
        };
      } else {
        if (event && event.serverResponse && event.serverResponse.response && event.serverResponse.response.body && imageContacts) {
          this.imageContacts_src = {
            path: event.serverResponse.response.body.path.replaceAll('\\', '/'),
            mimetype: event.serverResponse.response.body.mimetype,
            description: event.serverResponse.response.body.filename
          };
        }
      }


    }
  }

  removeImage() {
    this.image_src = {
      path: null,
      mimetype: null,
      description: null
    };
    this.editImage = [];
  }

  removeImageContacts() {
    this.imageContacts_src = {
      path: null,
      mimetype: null,
      description: null
    };
    this.editImageContacts = [];
  }


}
