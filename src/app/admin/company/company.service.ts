import {Injectable} from '@angular/core';
import {Observable} from "rxjs";
import {environment} from "../../../environments/environment";
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class CompanyService {

  constructor(private http: HttpClient) {
  }

  get(): Observable<any> {
    return this.http.get(environment.api_url + '/api/company');
  }

  getById(id): Observable<any> {
    return this.http.get(environment.api_url + `/api/company/${id}`);
  }

  createCompany(company, options): Observable<any> {
    return this.http.post(environment.api_url + '/api/company', company, options);
  }

  updateCompany(id, company, options): Observable<any> {
    return this.http.put(environment.api_url + `/api/company/${id}`, company, options);
  }

  removeCompany(id, options): Observable<any> {
    return this.http.delete(environment.api_url + `/api/company/${id}`, options);
  }
}
