import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {CreateOrganizationComponent} from "./create-organization/create-organization.component";

const routes: Routes = [
  {
    path: '',
    component: CreateOrganizationComponent,
    data: {name: 'Organization'},
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OrganizationRoutingModule {
}

export const routedComponents = [
  CreateOrganizationComponent
];
