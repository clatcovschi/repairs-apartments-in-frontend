import {Component, OnInit, TemplateRef} from '@angular/core';
import {NbDialogService} from "@nebular/theme";
import {HttpClient} from "@angular/common/http";
import {AccessTokenService} from "../../consts/access-token.service";
import {ActivatedRoute, Router} from "@angular/router";
import {OrganizationService} from "../organization.service";
import {environment} from "../../../../environments/environment";

@Component({
  selector: 'app-create-organization',
  templateUrl: './create-organization.component.html',
  styleUrls: ['./create-organization.component.scss']
})
export class CreateOrganizationComponent implements OnInit {

  image_src = [];
  file;
  imageUpload;
  editImage = [];
  add_organization = false;
  isEditPage = false;
  columns: {}
  response;
  data;
  id;
  organizations = [
    {
      title: '',
      description: '',
      mainProductImage: {}
    }
  ];
  object = {
    title: '',
    description: '',
    organizations: [],
  };

  constructor(private dialogService: NbDialogService,
              private http: HttpClient,
              public accessToken: AccessTokenService,
              private route: ActivatedRoute,
              private router: Router,
              private service: OrganizationService) {

    this.file = []

    route.queryParams.subscribe(p => {

      this.editImage = []
      this.id = p.Id;
      if (this.id !== undefined) {
        this.isEditPage = true;
        this.service.getById(this.id).subscribe(data => {
          this.object = data;
          this.organizations = data.organizations;
          for (let i = 0; i < data.organizations.length; i++) {
            if (data.organizations[i].mainProductImage && data.organizations[i].mainProductImage.path) {
              this.image_src[i] = data.organizations[i].mainProductImage;
              this.editImage[i] = [{
                url: `${environment.api_url}/api/` + data.organizations[i].mainProductImage.path.replaceAll('\\', '/'),
                fileName: data.organizations[i].mainProductImage.description
              }];

            }
          }


        });
      }
    });
  }


  ngOnInit(): void {
    this.imageUpload = `${environment.api_url}/api/upload/organization/image`;

    this.service.get().subscribe(data => {
      this.response = data.docs;
      if (data.docs.length === 0) this.add_organization = true;
    });

    this.columns = {
      id: {
        title: 'ID'
      },
      title: {
        title: 'TITLE'
      },
    }

  }

  addOrganization() {
    this.organizations.push({
      title: '',
      description: '',
      mainProductImage: {}
    })
  }

  deleteOrganization(index) {
    this.organizations.splice(index, 1);
    this.image_src = [];
    this.editImage = [];
  }

  open(dialog: TemplateRef<any>) {
    this.editImage = [];
    this.image_src = [];
    this.organizations = [
      {
        title: '',
        description: '',
        mainProductImage: {}
      }
    ];
    this.dialogService.open(dialog, {});
    this.router.navigate(
      ['.'],
      {relativeTo: this.route}
    );
    this.isEditPage = false;
    this.object = {
      title: '',
      description: '',
      organizations: [],
    };
  }


  onSubmit() {

    this.object.organizations = this.organizations;

    if (this.isEditPage) {
      this.service.updateOrganization(JSON.stringify(this.object), this.id, {headers: this.accessToken.headers}).subscribe(res => {
          this.service.get().subscribe(data => {
            this.response = data.docs;
          });
          this.service.getById(this.id).subscribe(data => {
            this.organizations = data.organizations;
            // @ts-ignore
            for (let i = 0; i < data.organizations.length; i++) {
              this.editImage[i] = [{
                url: `${environment.api_url}/api/` + data.organizations[i].mainProductImage.path.replaceAll('\\', '/'),
                fileName: data.organizations[i].mainProductImage.description
              }];

            }
          });

        }
      );
    } else {
      this.add_organization = false;
      this.service.createOrganization(JSON.stringify(this.object), {headers: this.accessToken.headers}).subscribe(res => {
        this.service.get().subscribe(data => {
          this.response = data.docs;
        });
      });
    }
  }

  uploadFinished(event, i) {

    if (event && event.serverResponse && event.serverResponse.response && event.serverResponse.response.body) {
      this.organizations[i].mainProductImage = {
        path: event.serverResponse.response.body.path.replaceAll('\\', '/'),
        mimetype: event.serverResponse.response.body.mimetype,
        description: event.serverResponse.response.body.filename
      }
    }
  }

  removeImage(i) {
    this.organizations[i].mainProductImage = {};
    this.editImage = []
  }
}
