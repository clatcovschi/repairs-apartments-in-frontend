import {Component, OnInit, TemplateRef} from '@angular/core';
import {NbDialogService} from "@nebular/theme";
import {HttpClient} from "@angular/common/http";
import {AccessTokenService} from "../../consts/access-token.service";
import {ActivatedRoute, Router} from "@angular/router";
import {AboutUsService} from "../about-us.service";

@Component({
  selector: 'app-create-about-us',
  templateUrl: './create-about-us.component.html',
  styleUrls: ['./create-about-us.component.scss']
})
export class CreateAboutUsComponent implements OnInit {

  add_info = false;
  isEditPage = false;
  columns: {}
  response;
  data;
  id;
  object = {
    miniDescription: '',
    description: '',
  };

  constructor(private dialogService: NbDialogService,
              private http: HttpClient,
              public accessToken: AccessTokenService,
              private route: ActivatedRoute,
              private router: Router,
              private service: AboutUsService) {
    route.queryParams.subscribe(p => {
      this.id = p.Id;
      if (this.id !== undefined) {
        this.isEditPage = true;
        this.service.getById(this.id).subscribe(data => {
          this.object = data;
        });
      }
    });

  }

  open(dialog: TemplateRef<any>) {
    this.dialogService.open(dialog, {});
    this.router.navigate(
      ['.'],
      {relativeTo: this.route}
    );
    this.isEditPage = false;
    this.object = {
      miniDescription: '',
      description: '',
    };
  }

  ngOnInit(): void {

    this.service.get().subscribe(data => {
      this.response = data.docs;
      if (data.docs.length === 0) this.add_info = true;
    });

    this.columns = {
      id: {
        title: 'ID'
      },
      miniDescription: {
        title: 'Mini Description'
      }
    }

  }

  onSubmit() {


    if (this.isEditPage) {
      this.service.updateAboutUs(this.id, JSON.stringify(this.object), {headers: this.accessToken.headers}).subscribe(res => {
          this.service.get().subscribe(data => {
            this.response = data.docs;
          });
        }
      );
    } else {
      this.add_info = false;
      this.service.createAboutUs(JSON.stringify(this.object), {headers: this.accessToken.headers}).subscribe(res => {
        this.service.get().subscribe(data => {
          this.response = data.docs;
        });
      });
    }

  }
}
