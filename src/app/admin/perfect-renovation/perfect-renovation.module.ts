import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CreateRenovationComponent} from './create-renovation/create-renovation.component';
import {PerfectRenovationService} from "./perfect-renovation.service";
import {PerfectRenovationRoutingModule} from "./perfect-renovation-routing.module";
import {NbButtonModule, NbCardModule} from "@nebular/theme";
import {SharedModule} from "../shared/shared.module";
import {FormsModule} from "@angular/forms";


@NgModule({
  declarations: [CreateRenovationComponent],
  imports: [
    CommonModule,
    PerfectRenovationRoutingModule,
    NbCardModule,
    SharedModule,
    NbButtonModule,
    FormsModule,
  ],
  providers: [
    PerfectRenovationService
  ]
})
export class PerfectRenovationModule {
}
