import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {CreateRenovationComponent} from "./create-renovation/create-renovation.component";

const routes: Routes = [
  {
    path: '',
    component: CreateRenovationComponent,
    data: {name: 'PerfectRenovation'},
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PerfectRenovationRoutingModule {
}

export const routedComponents = [
  CreateRenovationComponent
];
