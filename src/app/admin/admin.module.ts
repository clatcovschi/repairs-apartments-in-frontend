import {NgModule} from '@angular/core';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {NbAuthJWTInterceptor} from '@nebular/auth';
import {AdminRoutingModule} from './admin-routing.module';
import {
  NbAlertModule,
  NbButtonModule,
  NbCardModule, NbCheckboxModule,
  NbDialogModule,
  NbDialogService, NbIconModule, NbInputModule,
  NbLayoutModule, NbListModule,
  NbMenuModule, NbSelectModule,
  NbSidebarModule, NbThemeModule, NbToastrModule
} from '@nebular/theme';
import {AdminComponent} from './admin.component';
import {AdminUserProfileService} from "./admin-user-profile.service";
import {TokenInterceptor} from "./admin-token-interceptor";
import {AccessTokenService} from "./consts/access-token.service";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {NbEvaIconsModule} from "@nebular/eva-icons";
import {UsersModule} from "./users/users.module";
import {SharedModule} from "./shared/shared.module";
import {ServicesModule} from "./services/services.module";
import {AboutUsModule} from "./about-us/about-us.module";
import {CompanyAdvantagesModule} from "./company-advantages/company-advantages.module";
import {PerfectRenovationModule} from "./perfect-renovation/perfect-renovation.module";
import {OrganizationModule} from "./organization/organization.module";
import {GoalsModule} from "./goals/goals.module";
import {PhotoAlbumsModule} from "./photo-albums/photo-albums.module";
import {MessagesModule} from "./messages/messages.module";


@NgModule({
  imports: [

    HttpClientModule,
    SharedModule,
    AdminRoutingModule,
    NbMenuModule.forRoot(),
    NbSidebarModule.forRoot(),
    NbListModule,
    FormsModule,
    NbCheckboxModule,
    NbSelectModule,
    ServicesModule,
    ReactiveFormsModule,
    NbSelectModule,
    NbDialogModule.forRoot(),
    NbToastrModule.forRoot(),
    NbEvaIconsModule,
    NbCardModule,
    NbThemeModule.forRoot({name: 'dark'}),
    NbLayoutModule,
    NbAlertModule,
    NbInputModule,
    NbButtonModule,
    UsersModule,
    NbIconModule,
    AboutUsModule,
    CompanyAdvantagesModule,
    PerfectRenovationModule,
    OrganizationModule,
    GoalsModule,
    PhotoAlbumsModule,
    MessagesModule
  ],
  declarations: [AdminComponent],
  providers: [
    {provide: HTTP_INTERCEPTORS, useClass: NbAuthJWTInterceptor, multi: true},
    AdminUserProfileService,
    TokenInterceptor,
    AccessTokenService,
    NbDialogService]
})


export class AdminModule {
}
