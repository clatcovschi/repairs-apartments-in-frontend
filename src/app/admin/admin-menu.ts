import {NbMenuItem} from '@nebular/theme';

export const MENU_ITEMS: NbMenuItem[] = [
  {
    title: 'Company',
    icon: '',
    link: '/admin/company',
  },
  {
    title: 'Company Advantages',
    icon: '',
    link: '/admin/company-advantages',
  },
  {
    title: 'Perfect Renovation',
    icon: '',
    link: '/admin/perfect-renovation',
  },
  {
    title: 'Organization',
    icon: '',
    link: '/admin/organization',
  },
  {
    title: 'Goals',
    icon: '',
    link: '/admin/goals',
  },
  {
    title: 'Services',
    icon: '',
    link: '/admin/services',
  },
  {
    title: 'Photo Albums',
    icon: '',
    link: '/admin/photo-albums',
  },
  {
    title: 'About us',
    icon: '',
    link: '/admin/about-us',
  },
  {
    title: 'Blog',
    icon: '',
    link: '/admin/blog',
  },
  {
    title: 'Messages',
    icon: '',
    link: '/admin/messages',
  },
  {
    title: 'Users',
    icon: '',
    link: '/admin/users',
  },
];
