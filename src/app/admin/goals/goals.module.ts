import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CreateGoalComponent} from './create-goal/create-goal.component';
import {GoalsService} from "./goals.service";
import {GoalsRoutingModule} from "./goals-routing.module";
import {NbButtonModule, NbCardModule} from "@nebular/theme";
import {SharedModule} from "../shared/shared.module";
import {FormsModule} from "@angular/forms";


@NgModule({
  declarations: [CreateGoalComponent],
  imports: [
    CommonModule,
    GoalsRoutingModule,
    NbCardModule,
    SharedModule,
    NbButtonModule,
    FormsModule,
  ],
  providers: [GoalsService]
})
export class GoalsModule {
}
