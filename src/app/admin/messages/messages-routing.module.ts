import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {SeeMessagesComponent} from "./see-messages/see-messages.component";

const routes: Routes = [
  {
    path: '',
    component: SeeMessagesComponent,
    data: {name: 'Messages'},
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MessagesRoutingModule {
}

export const routedComponents = [
  SeeMessagesComponent
];
