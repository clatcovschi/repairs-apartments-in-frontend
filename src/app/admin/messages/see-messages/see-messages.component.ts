import {Component, OnInit} from '@angular/core';
import {NbDialogService} from "@nebular/theme";
import {HttpClient} from "@angular/common/http";
import {AccessTokenService} from "../../consts/access-token.service";
import {ActivatedRoute, Router} from "@angular/router";
import {MessagesService} from "../messages.service";

@Component({
  selector: 'app-see-messages',
  templateUrl: './see-messages.component.html',
  styleUrls: ['./see-messages.component.scss']
})
export class SeeMessagesComponent implements OnInit {


  totalDocs;
  totalPages = [];
  page;
  isEditPage = false;
  columns: {}
  response;
  data;
  id;
  object;

  constructor(private dialogService: NbDialogService,
              private http: HttpClient,
              public accessToken: AccessTokenService,
              private route: ActivatedRoute,
              private router: Router,
              private service: MessagesService) {

    route.queryParams.subscribe(p => {
      if (p.page) {
        this.page = Number(p.page);
      } else {
        this.page = 1;
        this.router.navigate(['.'], {relativeTo: this.route, queryParams: {page: this.page}});
      }

      this.id = p.Id;
      if (this.id !== undefined) {
        this.isEditPage = true;
        this.service.getById({headers: accessToken.headers}, this.page, this.id).subscribe(data => {
          this.object = data;
        });
      }
    });

  }


  ngOnInit(): void {

    this.service.get({headers: this.accessToken.headers}, this.page).subscribe(data => {
      this.response = data.docs;
      this.totalDocs = data.totalDocs;
      for (let i = 0; i < data.totalPages; i++) {
        this.totalPages[i] = i + 1;
        this.totalDocs = data.totalDocs;
      }
    });

    this.columns = {
      id: {
        title: 'ID'
      },
      phone: {
        title: 'Phone'
      },
      message: {
        title: 'Message'
      },
    }

  }

  nextPage() {
    this.page += 1;
    this.router.navigate(['.'], {relativeTo: this.route, queryParams: {page: this.page}});
    this.service.get({headers: this.accessToken.headers}, this.page).subscribe(data => {
      this.response = data.docs;
    })
  }

  getPage(page) {
    this.page = Number(page);
    this.router.navigate(['.'], {relativeTo: this.route, queryParams: {page: this.page}});
    this.service.get({headers: this.accessToken.headers}, this.page).subscribe(data => {
      this.response = data.docs;
    })
  }

  prevPage() {
    this.page -= 1;
    this.router.navigate(['.'], {relativeTo: this.route, queryParams: {page: this.page}});
    this.service.get({headers: this.accessToken.headers}, this.page).subscribe(data => {
      this.response = data.docs;
    })
  }


}
