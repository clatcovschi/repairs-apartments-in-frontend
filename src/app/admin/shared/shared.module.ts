import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ThemeModule} from '../../@theme/theme.module';
import {SmartTableComponent} from "./smart-table/smart-table.component";
import {
  NbButtonModule,
  NbCheckboxModule,
  NbInputModule,
  NbSelectModule,
  NbThemeModule,
} from '@nebular/theme';
import {RouterModule} from '@angular/router';
import {Ng2SmartTableModule} from "ng2-smart-table";
import {QuillComponentComponent} from "./quill-component/quill-component.component";
import {QuillModule} from "ngx-quill";
import {FormsModule} from "@angular/forms";


@NgModule({
  imports: [ThemeModule, CommonModule, NbSelectModule, RouterModule, Ng2SmartTableModule, QuillModule.forRoot(),
    NbSelectModule,
    NbThemeModule.forRoot({name: 'dark'}),
    NbInputModule,
    NbButtonModule,
    NbCheckboxModule, FormsModule,],
  exports: [ThemeModule, CommonModule, SmartTableComponent, QuillComponentComponent],
  declarations: [SmartTableComponent, QuillComponentComponent]
})
export class SharedModule {
}
