import {Component, Input, OnInit, TemplateRef} from '@angular/core';
import {NbDialogService, NbToastrService} from "@nebular/theme";
import {HttpClient} from "@angular/common/http";
import {AccessTokenService} from "../../consts/access-token.service";
import {ActivatedRoute, Router} from "@angular/router";
import {environment} from "../../../../environments/environment";

@Component({
  selector: 'smart-table',
  templateUrl: './smart-table.component.html',
  styleUrls: ['./smart-table.component.scss'],
})
export class SmartTableComponent implements OnInit {
  settings;
  event;
  @Input() dialog;
  @Input() data;
  @Input() toast;
  @Input() page;
  @Input() edit;
  @Input() delete;
  @Input() src;
  @Input() columnsName: any;


  onDeleteConfirm(event) {
    if (window.confirm('Are you sure you want to delete?')) {
      this.showToast('top-right', 'success');
      for (let i = 0; i < event._dataSet.rows.length; i++) {
        if (this.edit) {
          if (event._dataSet.rows[i].data.id === event.data.id) {
            event._dataSet.rows.splice(i, 1);
          }
          this.http.delete<any>(`${environment.api_url}/api/${this.src}/${event.data.id}?page=${this.page}`, {headers: this.accessToken.headers}).subscribe(res => {
            this.http.get<any>(`${environment.api_url}/api/${this.src}?page=${this.page}`, {headers: this.accessToken.headers}).subscribe(res => {
              this.data = res.docs;
            });
          });
        } else {
          if (event._dataSet.rows[i].data._id === event.data._id) {
            event._dataSet.rows.splice(i, 1);
          }
          this.http.delete<any>(`${environment.api_url}/api/${this.src}/${event.data._id}?page=${this.page}`, {headers: this.accessToken.headers}).subscribe(res => {
            this.http.get<any>(`${environment.api_url}/api/${this.src}?page=${this.page}`, {headers: this.accessToken.headers}).subscribe(res => {
              this.data = res.docs;
            });
          });
        }
      }

    }
  }

  constructor(private dialogService: NbDialogService,
              private http: HttpClient,
              private toastrService: NbToastrService,
              private accessToken: AccessTokenService,
              private router: Router,
              private route: ActivatedRoute
  ) {

  }

  open(dialog: TemplateRef<any>, event) {
    this.router.navigate(['.'], {relativeTo: this.route, queryParams: {page: this.page, Id: event.data.id}});


    this.http.get<any>(`${environment.api_url}/api/${this.src}?page=${this.page}`, {headers: this.accessToken.headers}).subscribe(res => {
      for (let i = 0; i < res.docs.length; i++) {
        if (res.docs[i].id === event.data.id) {
          this.event = res.docs[i];
          this.dialogService.open(dialog, {
            context: this.event
          });
        }
      }

    });

  }

  ngOnInit(): void {
    this.settings = {
      actions: {
        position: 'right',
        add: true,
        delete: this.delete,
        edit: this.edit,
        columnTitle: '',
      }, edit: {
        editButtonContent: `<img width="24px" height="24px" src="https://upload.wikimedia.org/wikipedia/en/thumb/8/8a/OOjs_UI_icon_edit-ltr-progressive.svg/1200px-OOjs_UI_icon_edit-ltr-progressive.svg.png" alt="">`,
      },
      delete: {
        deleteButtonContent: '<img src="https://coffeein.md/assets/img/icons/delete-item.svg" alt="">',
      },
      columns: this.columnsName,
      mode: 'external',
      pager: {display: false}
    };

  }

  showToast(position, status) {
    this.toastrService.show(
      status || 'success',
      `${this.toast} removed successfully`,
      {position, status});
  }

}
