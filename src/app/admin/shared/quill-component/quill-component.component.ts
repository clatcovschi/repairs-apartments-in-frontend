import {Component, forwardRef, Input, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {noop} from "rxjs";
import {NG_VALUE_ACCESSOR} from "@angular/forms";
import * as QuillNamespace from 'quill';

let Quill: any = QuillNamespace;
import ImageResize from 'quill-image-resize-module';
import {environment} from "../../../../environments/environment";

Quill.register('modules/imageResize', ImageResize);


@Component({
  selector: 'app-quill-component',
  templateUrl: './quill-component.component.html',
  styleUrls: ['./quill-component.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => QuillComponentComponent),
      multi: true
    }
  ],
  encapsulation: ViewEncapsulation.None,


})


export class QuillComponentComponent implements OnInit {
  maxUploadFileSize = 10000000;
  @Input() formControlName;
  @Input() placeholder;
  @Input() nullToEmptyString: boolean;
  quillEditorRef;
  value;
  disabled = false;
  quillModules;
  ql_editor1;
  attrQuillTxtArea;
  quillCustomDiv;
  editorTouched = false;
  initialValue;
  private onTouched: () => void = noop;
  private onChange: (_: any) => void = noop;
  @ViewChild('editor') editor;


  ngOnInit() {


    this.quillModules = {
      toolbar: {
        container: [
          ['bold', 'italic', 'underline', 'strike'],        // toggled buttons
          ['blockquote', 'code-block'],

          [{'header': 1}, {'header': 2}],               // custom button values
          [{'list': 'ordered'}, {'list': 'bullet'}],
          [{'script': 'sub'}, {'script': 'super'}],      // superscript/subscript
          [{'indent': '-1'}, {'indent': '+1'}],          // outdent/indent
          [{'direction': 'rtl'}],                         // text direction
          [{'size': ['small', false, 'large', 'huge']}],  // custom dropdown
          [{'header': [1, 2, 3, 4, 5, 6, false]}],
          [{'color': ["#000000", "#e60000", "#ff9900", "#ffff00", "#008a00", "#0066cc", "#9933ff", "#ffffff", "#facccc", "#ffebcc", "#ffffcc", "#cce8cc", "#cce0f5", "#ebd6ff", "#bbbbbb", "#f06666", "#ffc266", "#ffff66", "#66b966", "#66a3e0", "#c285ff", "#888888", "#a10000", "#b26b00", "#b2b200", "#006100", "#0047b2", "#6b24b2", "#444444", "#5c0000", "#663d00", "#666600", "#003700", "#002966", "#3d1466"]}, {'background': []}],
          [{'font': []}],
          [{'align': []}],
          ['clean'],                                        // remove formatting button
          ['link', 'image', 'video']                         // link and image, video
        ]
      },
      imageResize: true,

    };



  }

  async setEvents(customButton): Promise<void> {
    if (customButton) {
      customButton.addEventListener('click', () => {
        const wasActiveTxtArea_1 =
          (this.ql_editor1.getAttribute('quill__html').indexOf('-active-') > -1);
        if (wasActiveTxtArea_1) {

          this.quillEditorRef.clipboard.dangerouslyPasteHTML(this.ql_editor1.value);
          customButton.classList.remove('ql-active');
        } else {

          this.ql_editor1.value = this.quillEditorRef.root.innerHTML;
          customButton.classList.add('ql-active');
        }

        this.ql_editor1.addEventListener('change', () => {
          this.quillEditorRef.clipboard.dangerouslyPasteHTML(this.ql_editor1.value);
        });

        this.ql_editor1.setAttribute('quill__html', wasActiveTxtArea_1 ? '' : '-active-');
      });
    }
  }

  async getEditorInstance(editorInstance: any) {
    this.quillEditorRef = editorInstance;

    this.ql_editor1 = document.createElement('textarea');
    this.attrQuillTxtArea = document.createAttribute('quill__html');
    this.ql_editor1.setAttributeNode(this.attrQuillTxtArea);

    this.quillCustomDiv = this.quillEditorRef.addContainer('ql-custom');
    this.quillCustomDiv.appendChild(this.ql_editor1);

    const toolbar = editorInstance.getModule('toolbar');
    const buttonElement = toolbar.controls.filter(control => control[0] === 'code-block')[0];
    this.setEvents(buttonElement[1]);

  }


  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  insertImageToEditor(res) {
    let url = res.filename;
    const range = this.quillEditorRef.getSelection();
    url = `${environment.api_url}/api/public/articles/images/` + url;
    const img = '<img src="' + url + '" />';
    this.quillEditorRef.clipboard.dangerouslyPasteHTML(range.index, img);
  }

  updateValue(insideValue: any) {
    if (this.editorTouched) {
      const quillData = {
        html: ''
      };
      if (this.nullToEmptyString) {
        quillData.html = insideValue.html === null ? '' : insideValue.html;
      } else {
        quillData.html = insideValue.html;
      }
      this.value = quillData.html;
      this.onChange(this.value);
      this.onTouched();
    }
  }

  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;

  }

  writeValue(obj: any): void {
    this.initialValue = obj;
    setTimeout(
      () => {
        if (this.quillEditorRef) {
          this.quillEditorRef.root.innerHTML = this.initialValue;
        }
      }, 200
    );
  }

  onEditorTouched() {
    this.editorTouched = true;
  }

}

