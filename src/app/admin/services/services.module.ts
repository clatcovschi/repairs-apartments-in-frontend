import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ServicesService} from "./services.service";
import {ServicesRoutingModule} from "./services-routing.module";
import {NbButtonModule, NbCardModule} from "@nebular/theme";
import {SharedModule} from "../shared/shared.module";
import {FormsModule} from "@angular/forms";
import {ImageUploadModule} from "angular2-image-upload";
import {routedComponents} from "./services-routing.module";

@NgModule({
  declarations: [...routedComponents],
  imports: [
    CommonModule,
    ServicesRoutingModule,
    NbCardModule,
    SharedModule,
    NbButtonModule,
    FormsModule,
    ImageUploadModule.forRoot(),
  ],
  providers: [ServicesService]
})
export class ServicesModule {
}
