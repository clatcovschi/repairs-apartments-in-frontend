import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CreateAlbumComponent} from './create-album/create-album.component';
import {PhotoAlbumsService} from "./photo-albums.service";
import {PhotoAlbumsRoutingModule} from "./photo-albums-routing.module";
import {NbButtonModule, NbCardModule} from "@nebular/theme";
import {SharedModule} from "../shared/shared.module";
import {FormsModule} from "@angular/forms";
import {ImageUploadModule} from "angular2-image-upload";


@NgModule({
  declarations: [CreateAlbumComponent],
  imports: [
    CommonModule,
    PhotoAlbumsRoutingModule,
    NbCardModule,
    SharedModule,
    NbButtonModule,
    FormsModule,
    ImageUploadModule.forRoot(),
  ],
  providers: [
    PhotoAlbumsService
  ]
})
export class PhotoAlbumsModule {
}
