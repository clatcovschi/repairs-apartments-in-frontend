import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {CreateBlogComponent} from "./create-blog/create-blog.component";

const routes: Routes = [
  {
    path: '',
    component: CreateBlogComponent,
    data: {name: 'Blog'},
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BlogRoutingModule {
}

export const routedComponents = [
  CreateBlogComponent
];
