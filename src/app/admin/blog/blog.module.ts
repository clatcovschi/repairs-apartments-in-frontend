import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CreateBlogComponent} from './create-blog/create-blog.component';
import {BlogRoutingModule} from "./blog-routing.module";
import {NbButtonModule, NbCardModule} from "@nebular/theme";
import {SharedModule} from "../shared/shared.module";
import {FormsModule} from "@angular/forms";
import {ImageUploadModule} from "angular2-image-upload";
import {BlogService} from "./blog.service";


@NgModule({
  declarations: [CreateBlogComponent],
  imports: [
    CommonModule,
    BlogRoutingModule,
    NbCardModule,
    SharedModule,
    NbButtonModule,
    FormsModule,
    ImageUploadModule.forRoot(),
  ],
  providers: [BlogService]
})
export class BlogModule {
}
