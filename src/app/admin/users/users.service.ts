import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {Observable} from 'rxjs';

@Injectable()
export class UserService {

  constructor(private http: HttpClient) {
  }

  get(options, page): Observable<any> {
    return this.http.get(environment.api_url + `/api/users?page=${page}`, options);
  }

  createUser(user, options): Observable<any> {
    return this.http.post(environment.api_url + '/api/users?limit=9999999999999999999', user, options);
  }
}
