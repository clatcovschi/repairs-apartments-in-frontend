import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CreateCompanyAdvantagesComponent} from './create-company-advantages/create-company-advantages.component';
import {CompanyAdvantagesRoutingModule} from "./company-advantages-routing.module";
import {NbButtonModule, NbCardModule} from "@nebular/theme";
import {SharedModule} from "../shared/shared.module";
import {FormsModule} from "@angular/forms";
import {CompanyAdvantagesService} from "./company-advantages.service";
import {ImageUploadModule} from "angular2-image-upload";


@NgModule({
  declarations: [CreateCompanyAdvantagesComponent],
  imports: [
    CommonModule,
    CompanyAdvantagesRoutingModule,
    NbCardModule,
    SharedModule,
    NbButtonModule,
    FormsModule,
    ImageUploadModule.forRoot(),
  ],
  providers: [CompanyAdvantagesService]
})
export class CompanyAdvantagesModule {
}
