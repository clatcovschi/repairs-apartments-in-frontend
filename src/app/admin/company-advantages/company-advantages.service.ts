import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {environment} from "../../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class CompanyAdvantagesService {
  constructor(private http: HttpClient) {
  }

  get(page): Observable<any> {
    return this.http.get(environment.api_url + `/api/company-advantages?page=${page}`);
  }

  getById(id, page): Observable<any> {
    return this.http.get(environment.api_url + `/api/company-advantages/${id}?page=${page}`);
  }

  createCompanyAdvantage(company_advantage, options): Observable<any> {
    return this.http.post(environment.api_url + '/api/company-advantages?limit=9999999999999999999', company_advantage, options);
  }

  updateCompanyAdvantage(company_advantage, id, options): Observable<any> {
    return this.http.put(environment.api_url + `/api/company-advantages/${id}`, company_advantage, options);
  }
}
