import { Injectable } from '@angular/core';
import {Router} from "@angular/router";
import {NbTokenService} from "@nebular/auth";

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private tokenService: NbTokenService, private router: Router, private NbJwt: NbTokenService) {
  }

  public isAuthenticated(): boolean {
    const token = this.NbJwt.get();
    return !!token;
  }

  logout() {
    this.tokenService.clear();
    this.router.navigate(['auth/login']);
  }

}
